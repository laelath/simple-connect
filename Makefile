sources := main.cc
objects := $(addprefix ., $(sources:.cc=.o))
deps    := $(addprefix ., $(sources:.cc=.d))

CPPFLAGS += -DASIO_STANDALONE

all: simple-connectd

simple-connectd: $(objects)
	$(CXX) $(LDFLAGS) $(CXXFLAGS) $(objects) $(LIBS) -o $@ -pthread

-include $(deps)

.%.o: %.cc
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -MD -MP -MF $(addprefix ., $(<:.cc=.d)) -c -o $@ $< -pthread
