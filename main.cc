#include <ctime>
#include <functional>
#include <iostream>
#include <string>

#include <asio.hpp>

using asio::ip::tcp;

std::string make_daytime_string()
{
    std::time_t now = std::time(0);
    return std::ctime(&now);
}

class tcp_connection : public std::enable_shared_from_this<tcp_connection>
{
public:
    tcp_connection(asio::io_service& io_service)
        : m_socket(io_service)
    {
    }

    tcp::socket& socket()
    {
        return m_socket;
    }

    void start()
    {
        m_message = make_daytime_string();

        asio::async_write(m_socket, asio::buffer(m_message),
                          std::bind(&tcp_connection::handle_write, shared_from_this(),
                                    std::placeholders::_1, std::placeholders::_2));
    }

private:
    void handle_write(const asio::error_code& /*error*/, size_t /*transferred*/)
    {
    }

    tcp::socket m_socket;
    std::string m_message;
};

class tcp_server
{
public:
    tcp_server(asio::io_service& io_service)
        : m_acceptor(io_service, tcp::endpoint(tcp::v4(), 3742))
    {
        start_accept();
    }

private:
    void start_accept()
    {
        auto new_connection = std::make_shared<tcp_connection>(m_acceptor.get_executor().context());
        m_acceptor.async_accept(new_connection->socket(),
                               std::bind(&tcp_server::handle_accept, this, new_connection,
                                         std::placeholders::_1));
    }

    void handle_accept(std::shared_ptr<tcp_connection> new_connection, const asio::error_code& error)
    {
        if (!error) {
            new_connection->start();
        }

        start_accept();
    }

    tcp::acceptor m_acceptor;
};


int main(int argc, char *argv[])
{
    try {
        asio::io_service io_service;
        tcp_server server(io_service);
        io_service.run();
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
